﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerWebService
{
    public class Log
    {
        DataClassesDataContext db;

        public Log()
        {
            db = new DataClassesDataContext();
        }

        public enum LogType
        {
            Init,
            Start,
            Stop,
            LinkAdded,
            LinkCrawled
        }

        public static String LogTypeToString(LogType logType)
        {
            switch (logType)
            {
                case LogType.Init: return "Init Crawler";
                case LogType.Start: return "Start Crawler";
                case LogType.Stop: return "Stop Crawler";
                case LogType.LinkAdded: return "Added new Link";
                case LogType.LinkCrawled: return "Crawled Link";
                default: return "Unknown log type!";
            }
        }

        public void LogInit(bool success, string link)
        {
            LogGeneric(LogType.Init, success, link, null);
        }

        public void LogStart(bool success)
        {
            LogGeneric(LogType.Start, success, null, null);
        }

        public void LogStop(bool success)
        {
            LogGeneric(LogType.Stop, success, null, null);
        }

        public void LogLinkAdded(bool success, string link)
        {
            LogGeneric(LogType.LinkAdded, success, link, null);
        }

        public void LogLinkCrawled(bool success, string link, string crawler)
        {
            LogGeneric(LogType.LinkCrawled, success, link, crawler);
        }

        private void LogGeneric(LogType logType, bool success, string link, string crawler)
        {
            crawler_Log log = new crawler_Log();
            log.logtype = logType;
            log.success = success;
            log.url = link;
            log.crawlername = crawler;
            db.crawler_Logs.InsertOnSubmit(log);
            db.SubmitChanges();
        }
    }
}
