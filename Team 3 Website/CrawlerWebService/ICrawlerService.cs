﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace CrawlerWebService
{
    [ServiceContract]
    public interface ICrawlerService
    {
        [OperationContract]
        string HelloWorld();

        [OperationContract]
        bool InitCrawling(String _url);
        
        [OperationContract]
        bool StartCrawling();

        [OperationContract]
        bool StopCrawling();

        [OperationContract]
        bool IsCrawling();

        [OperationContract]
        bool IsInitialized();
    }

}
