﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace CrawlerWebService.Parser
{
    public class GenericParser : AbstractParser
    {
        public enum LinkType
        {
            External,
            OtherSubdomain,
            SameDomain
        }

        private LinkType followBehaviour;
        private DataClassesDataContext db;

        public GenericParser(LinkType followBehaviour)
        {
            this.followBehaviour = followBehaviour;
            db = new DataClassesDataContext();
        }

        public override bool IsResponsible(Uri url)
        {
            return true;
        }

        public override List<Uri> Parse(Uri url, HtmlDocument document)
        {
            List<Uri> links = new List<Uri>();

            foreach (HtmlNode link in document.DocumentNode.SelectNodes("//a[@href]"))
            {
                

                try
                {
                    Uri l = new Uri(url, link.GetAttributeValue("href", "NO_REFERENCE"));

                    if (followLink(url, l) && (IsUniqueLink(links, l)))
                        links.Add(l);
                }
                catch (UriFormatException)
                {
                    // only insert log on failure (success is handled in Crawler.cs)
                    // here it makes sense to make a new Log instance everytime it is needed, as this should only be the case very rarely
                    new Log().LogLinkAdded(false, link.GetAttributeValue("href", "NO_REFERENCE"));
                }
            }

            return links;
        }

        private bool IsUniqueLink(List<Uri> listUri, Uri u)
        {
            return !listUri.Any(uri => uri.ToString().ToLower() == u.ToString().ToLower()); // we have to compare lowercase, as keys in SQL are key-insensitive
        }


        public virtual bool followLink(Uri source, Uri target)
        {
            if (!(target.Scheme == "http" || target.Scheme == "https"))
                return false;

            // all links are followed
            if (followBehaviour == LinkType.External)
                return true;

            LinkType type = linkType(source, target);

            // only same domain or other subdomain are followed
            if (followBehaviour == LinkType.OtherSubdomain)
                return type == LinkType.OtherSubdomain || type == LinkType.SameDomain;

            // only same domain is followed
            if (followBehaviour == LinkType.SameDomain)
                return type == LinkType.SameDomain;

            // should not happen
            throw new NotImplementedException();
        }

        private LinkType linkType(Uri source, Uri target)
        {
            if (source.Host == target.Host)
                return LinkType.SameDomain;
            
            string[] sourceSplit = source.Host.Split('.');
            string[] targetSplit = target.Host.Split('.');

            if (sourceSplit[sourceSplit.Count() - 1] == targetSplit[targetSplit.Count() - 1]
                &&
                sourceSplit[sourceSplit.Count() - 2] == targetSplit[targetSplit.Count() - 2])
                return LinkType.OtherSubdomain;

            return LinkType.External;
        }
    }
}
