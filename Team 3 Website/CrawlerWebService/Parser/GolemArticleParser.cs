﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CrawlerWebService.Parser
{
    public class GolemArticleParser : GolemParser
    {
        DataClassesDataContext db;

        public GolemArticleParser()
        {
            db = new DataClassesDataContext();

        }

        public override bool IsResponsible(Uri url)
        {
            return (url.Host == "golem.de" || url.Host == "www.golem.de")
                && (url.LocalPath.StartsWith("/news/"));
        }

        public override List<Uri> Parse(Uri url, HtmlDocument document)
        {
            if (CrawlerMain.DEBUG)
                Console.WriteLine("prsing news site: " + url.ToString());

            if (!db.crawler_Golem_News.Any(g => g.url == url.ToString()))
            {
                crawler_Golem_News news = new crawler_Golem_News();
                document.OptionDefaultStreamEncoding = Encoding.UTF8;      
                news.url = url.ToString();

                var subheadlineNode = document.DocumentNode.SelectSingleNode("//h1/span[@class='dh2 head1']");
                if (subheadlineNode != null)
                    news.subheadline = subheadlineNode.InnerHtml;

                var headlineNode = document.DocumentNode.SelectSingleNode("//h1/span[@class='dh1 head5']");
                if (headlineNode != null)
                    news.headline = headlineNode.InnerHtml;

                var authorNode = document.DocumentNode.SelectSingleNode("//*[@rel='author']");
                if (authorNode != null)
                    news.author = authorNode.InnerHtml;

                foreach (HtmlNode node in document.DocumentNode.SelectNodes("//article//p"))
                {
                    news.article += HttpUtility.HtmlDecode(node.InnerHtml);
                }
                
                
                db.crawler_Golem_News.InsertOnSubmit(news);

                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    if(CrawlerMain.DEBUG)
                        Console.WriteLine("Duplicate news entry!");
                }

    
            }

              return base.Parse(url, document);
        }
    }
}
