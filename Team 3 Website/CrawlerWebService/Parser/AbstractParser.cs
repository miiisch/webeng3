﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerWebService.Parser
{
    public abstract class AbstractParser
    {
        public abstract bool IsResponsible(Uri url);
        public abstract List<Uri> Parse(Uri url, HtmlDocument document);
    }
}
