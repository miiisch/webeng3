﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CrawlerWebService.Parser
{
    public class GolemParser : GenericParser
    {
        public GolemParser() :
            base(LinkType.SameDomain)
        {
        }

        public override bool IsResponsible(Uri url)
        {
            return (url.Host == "golem.de" || url.Host == "www.golem.de");
        }

        public override bool followLink(Uri source, Uri target)
        {
            return base.followLink(source, target) && !target.LocalPath.StartsWith("/mail.php") 
                && !target.LocalPath.StartsWith("/print.php") 
                && !target.LocalPath.StartsWith("/trackback/")
                && !Regex.IsMatch(target.LocalPath, @"^/\d+/*"); // duplicate addresses for articles? (of form "golem.de/0123/45678(-2).html")
        }
    }
}
