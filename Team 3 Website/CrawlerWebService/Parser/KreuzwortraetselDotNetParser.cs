﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CrawlerWebService.Parser
{
    class KreuzwortraetselDotNetParser : GenericParser
    {
        private DataClassesDataContext db;

        public KreuzwortraetselDotNetParser() :
            base(LinkType.SameDomain)
        {
            db = new DataClassesDataContext();
        }

        public override bool IsResponsible(Uri url)
        {
            return url.Host == "www.kreuzwort-raetsel.net" && CanParseLocalPath(url.LocalPath);
        }

        public override bool followLink(Uri source, Uri target)
        {
            return base.followLink(source, target)
                && (CanParseLocalPath(target.LocalPath));
        }

        public override List<Uri> Parse(Uri url, HtmlAgilityPack.HtmlDocument document)
        {
            try
            {
                foreach (HtmlNode tableRow in document.DocumentNode.SelectNodes("//*[@id='column3']/table//tr[not(contains(@class, 'tblhead'))]"))
                {
                    crawler_Word word = new crawler_Word();

                    word.description = tableRow.SelectSingleNode("td[1]/a[1]").InnerText;
                    word.word = tableRow.SelectSingleNode("td[2]/a[2]").InnerText;

                    db.crawler_Words.InsertOnSubmit(word);
                }

                db.SubmitChanges();

            }
            catch (NullReferenceException)
            {
                Console.WriteLine("The page " + url + " does not have the expected structure!");
            }

            return base.Parse(url, document);
        }

        private bool CanParseLocalPath(string LocalPath)
        {
            return Regex.IsMatch(LocalPath, @"^/([a-z]|(sonstige))((-.\d+)|$)");
            /* matches:
             * /a
             * /a-[digits]
             * /b
             * /b-[digits]
             * ...
             * /z
             * /z-[digits]
             * /sonstige
             * /sonstige-[digits]
             */
        }
    }
}
