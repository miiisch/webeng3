﻿using CrawlerWebService.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services;

namespace CrawlerWebService
{
    class CrawlerMain
    {
        public static Boolean DEBUG = true;

        static void Main()
        {
            Console.WriteLine("This is the crawler from Webeng Team 3");

            Crawler crawler = new Crawler();
            // order of adding parsers is important! First come first served!
            // e.g. GolemArticleParser parses golem.de/news/*
            //      GolemParser        parses golem.de/*
            // => GolemArticleParser has to be added before GolemParser, else GolemArticleParser will never be called!
            crawler.addParser(new GolemArticleParser());
            crawler.addParser(new GolemParser());
            crawler.addParser(new KreuzwortraetselDotNetParser());

            CrawlerService crawlerInterface = new CrawlerService(crawler, new Uri("http://localhost:8000/crawler"));
        }
    }
}
