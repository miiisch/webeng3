﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services;

namespace CrawlerWebService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    class CrawlerService : ICrawlerService
    {
        private Crawler crawler;

        public CrawlerService(Crawler crawler, Uri baseAddress)
        {
            this.crawler = crawler;

            using (ServiceHost host = new ServiceHost(this, baseAddress))
            {
                host.Open();

                Console.WriteLine("The service is ready at {0}", baseAddress);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                // Close the ServiceHost.
                host.Close();
            }
        }

        public string HelloWorld()
        {
            return "Hello World";
        }

        public bool InitCrawling(String url)
        {
            return crawler.Init(url);
        }

        public bool IsCrawling()
        {
            return crawler.IsCrawling;
        }

        public bool StartCrawling()
        {
            return crawler.StartCrawling();
        }

        public bool StopCrawling()
        {
            return crawler.StopCrawling();
        }

        public bool IsInitialized()
        {
            return crawler.IsInitialized();
        }
    }
}
