﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using CrawlerWebService.Parser;
using System.Timers;
using System.Data.Linq;
using System.Data.SqlClient;

namespace CrawlerWebService
{
    class Crawler
    {
        DataClassesDataContext db;
        private Log log;
        private List<AbstractParser> parsers;
        private Timer timer;

        public Crawler(int politenessMS = 5000)
        {
            timer = new Timer(politenessMS);
            timer.Elapsed += new ElapsedEventHandler(crawlNext);

            // for now only restart manually after fetching is done
            // otherwise we would have to make sure first that nothing bad happens if two fetches are done in parallel
            timer.AutoReset = false;

            db = new DataClassesDataContext();
            log = new Log();

            IsCrawling = false;

            parsers = new List<AbstractParser>();
        }

        public Boolean IsCrawling { get; private set; }

        public bool Init(String seed)
        {
            bool success = !IsCrawling;

            try
            {
                Uri uri = new Uri(seed);
            }
            catch (UriFormatException)
            {
                success = false;
            }

            log.LogInit(success, seed);

            if (success)
            {
                // clear database (fast clear is not possible, as LINQ caches entries and can then produce DuplicateKeyException when inserting seed)
                db.crawler_Uris.DeleteAllOnSubmit(db.crawler_Uris);
                db.SubmitChanges();
                

                // insert seed
                crawler_Uri s = new crawler_Uri();
                s.uri = seed;
                s.addedOn = DateTime.Now;
                db.crawler_Uris.InsertOnSubmit(s);
                db.SubmitChanges();
            }

            return success;
        }

        public bool StartCrawling()
        {
            bool success = !IsCrawling;

            log.LogStart(success);

            if (success)
            {
                IsCrawling = true;
                crawlNext();
            }

            return success;
        }

        public bool StopCrawling()
        {
            bool success = IsCrawling;

            if (success)
            {
                IsCrawling = false;
                timer.Stop();
            }

            log.LogStop(success);

            return success;
        }

       

        public void addParser(AbstractParser parser)
        {
            parsers.Add(parser);
        }

        void crawlNext()
        {
            var all = db.crawler_Uris
                .Where(row => row.lastCrawled == null) // only select non-crawled links
                .OrderBy(row => row.addedOn) // sort by the date of adding the link (oldest links are first)
                .ThenBy(row => row.uri); // then sort alphabetically

            if (!all.Any())
                return;

            var first = all.First();

            Uri uri = null;
            try
            {
                uri = new Uri(first.uri);
            }
            catch (UriFormatException)
            {
                log.LogLinkCrawled(false, first.uri, null);
            }


            if (uri != null)
            {
                List<Uri> links = crawl(uri);
                // here we could add a global method which validates which links are valid
                // not sure if necessary, as the parsers already filter themselves



                foreach (Uri link in links)
                {
                    // insert if not present
                    if (!db.crawler_Uris.Any(c => c.uri == link.ToString()))
                    {
                        var s = new crawler_Uri();
                        s.uri = link.ToString();
                        s.addedOn = DateTime.Now;
                        db.crawler_Uris.InsertOnSubmit(s);

                        log.LogLinkAdded(true, link.ToString());
                    }
                }
            }

            first.lastCrawled = DateTime.Now;

            try
            {
                db.SubmitChanges();
            }
            catch (DuplicateKeyException)
            {
                Console.WriteLine("Duplicate Key Exception!");
            }
            catch (SqlException)
            {
                Console.WriteLine("Duplicate Key Exception!");
            }
            

            if (IsCrawling)
                timer.Start();
        }

        void crawlNext(object source, ElapsedEventArgs e)
        {
            crawlNext();
        }

        List<Uri> crawl(Uri url)
        {


            foreach (AbstractParser parser in parsers)
                if (parser.IsResponsible(url))
                {
                    HtmlDocument document = fetch(url);
                    if (document == null)
                    {
                        log.LogLinkCrawled(false, url.ToString(), parser.GetType().ToString()); // error while fetching
                        return new List<Uri>();
                    }
                    else
                    {
                        log.LogLinkCrawled(true, url.ToString(), parser.GetType().ToString()); // success
                        return parser.Parse(url, document);
                    }
                }

            log.LogLinkCrawled(false, url.ToString(), null); // no matching parser found

            return new List<Uri>();
        }

        HtmlDocument fetch(Uri uri)
        {
            var request = (HttpWebRequest)WebRequest.Create(uri);
            var cookieContainer = new CookieContainer();

            request.CookieContainer = cookieContainer;
            request.UserAgent = @"Web Engineering - Team 3: Web Crawler";
            request.Method = "GET";
            request.AllowAutoRedirect = true;
            request.Timeout = 15000;
            HttpWebResponse response;
            
            try
            {
               response  = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                Console.WriteLine(e.ToString()); 
                return null;
            }
            
            var page = new HtmlDocument();
            var stream = response.GetResponseStream();
            page.Load(stream, Encoding.UTF8);

            return page;
        }

        internal bool IsInitialized()
        {
            return db.crawler_Uris.Count() != 0;
        }
    }
}
