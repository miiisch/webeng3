﻿<%@ Page Title="Crawler Links" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrawlerLinks.aspx.cs" Inherits="Team_3_Website.CrawlerLinks1" %>
<%@ Register TagPrefix="My" TagName="CrawlerLinks" Src="~/CrawlerLinks.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    Auf dieser Seite werden alle dem Crawler bekannte Links aufgelistet. 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <My:CrawlerLinks runat="server"/>
</asp:Content>
