﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Team_3_Website
{
    public partial class GolemNews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CountLabel.Text = "Total: " + new DataClassesDataContext().crawler_Golem_News.Count().ToString() + " news entries";
        }
    }
}