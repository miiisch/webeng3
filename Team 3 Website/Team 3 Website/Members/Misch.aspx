﻿<%@ Page Title="Misch" Language="C#" MasterPageFile="~/Members/Members.master" AutoEventWireup="true" CodeBehind="Misch.aspx.cs" Inherits="Team_3_Website.Misch" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MemberContent">
    <asp:Image ID="Image" runat="server" CssClass="padding" Height="300px" ImageAlign="Left" ImageUrl="~/Images/misch.png" />

    <p>        
        <h3>Lebenslauf:</h3><br />
        Studiert Informatik am KIT in Karlsruhe
    </p>

    <p>        
        <h3>E-Mail:</h3><br />
        uni@miiis.ch
    </p>

    <p>        
        <h3>Hobbies</h3><br />
        Diving<br />
        Geocaching<br />
        Drummer
    </p>
</asp:Content>