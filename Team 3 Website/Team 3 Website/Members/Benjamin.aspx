﻿<%@ Page Title="Benjamin" Language="C#" MasterPageFile="~/Members/Members.master" AutoEventWireup="true" CodeBehind="Benjamin.aspx.cs" Inherits="Team_3_Website.Benjamin" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MemberContent">
    <asp:Image ID="Image1" runat="server" CssClass="padding" Height="300px" ImageAlign="Left" ImageUrl="~/Images/benjamin.png" />

    <p>        
        <h3>Lebenslauf:</h3><br />
        Geboren: 20.12.1989 bei Magdeburg<br />
        <br />
        Bachelor-Studium von 2008-2012 in Magdeburg<br />
        Master Studium von 2012-2014 in Karlsruhe
    </p>

    <p>        
        <h3>E-Mail:</h3><br />
        benjamin.espe@gmail.com
    </p>

    <p>        
        <h3>Hobbies</h3><br />
        Triathlon<br />
        Handball<br />
        Surfen<br />
        Fotografie<br />
        Schlagzeug<br />
        Geocaching
    </p>
</asp:Content>