﻿<%@ Page Title="Crawler" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Crawler.aspx.cs" Inherits="Team_3_Website.Crawler" %>
<%@ Register TagPrefix="My" TagName="CrawlerControl" Src="~/CrawlerWS/WS_Control.ascx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
   Auf dieser Seite kann der Crawler gesteuert werden. 
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <My:CrawlerControl runat="server" ID="crawler" />
</asp:Content>