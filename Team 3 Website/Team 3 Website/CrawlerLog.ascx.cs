﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Team_3_Website
{
    public partial class CrawlerLog : System.Web.UI.UserControl
    {
        CrawlerLogDataSource dataSource;

        public CrawlerLog()
        {
            dataSource = new CrawlerLogDataSource();
        }

        protected void LogDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = dataSource;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MinTimestampTextBox.Text = DateTime.Now.AddHours(-1).ToString();
                MaxTimestampTextBox.Text = DateTime.Now.ToString();
            }

            UpdateFilters();
            UpdateCount();
        }

        protected void UpdateFiltersButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
                UpdateFilters();
        }

        private void UpdateFilters()
        {
            dataSource.filter = new CrawlerLogDataSource.Filter(
                    InitCheckBox.Checked,
                    StartCheckBox.Checked,
                    StopCheckBox.Checked,
                    LinkAddedCheckBox.Checked,
                    LinkCrawledCheckBox.Checked,
                    DateTime.Parse(MinTimestampTextBox.Text),
                    DateTime.Parse(MaxTimestampTextBox.Text));

            DataBind(); // refresh
        }

        private void UpdateCount()
        {
            CountLabel.Text = "Showing " + dataSource.select().Rows.Count.ToString() + " logs";
            // TODO make a more efficient count method
        }
    }
}