﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WS_Control.ascx.cs" Inherits="Team_3_Website.CrawlerWS.WS_Control" %>

<h3>WebService Control:</h3>

<asp:Label ID="SuccessLabel" runat="server" ViewStateMode="Disabled" CssClass="success"></asp:Label>
<asp:Label ID="ErrorLabel" runat="server" ViewStateMode="Disabled" CssClass="error"></asp:Label>
<asp:Panel ID="InitPanel" runat="server">
    <p>
        You can use the Init control to reset the parser and add a new Seed. <br />
        Seed: <asp:TextBox id="urlInput" Text="" runat="server"/>
        <asp:Button id="InitButton" OnClick="InitButton_Click" Text="Init" runat="server" />
    </p>
</asp:Panel>
<asp:Panel ID="Empty" runat="server">
    <p>
        The frontier of the crawler is currently empty. To start crawling, you have to init the parser first. 
    </p>
</asp:Panel>
<asp:Panel ID="StartPanel" runat="server">
    <p>
        The crawler is ready to crawl, just press Start, to get going!
        <asp:Button id="StartButton" OnClick="StartButton_Click" Text="Start" runat="server" />
    </p>
</asp:Panel>
<asp:Panel ID="StopPanel" runat="server">
    <p>
        <b>The crawler is currently crawling!</b><br />
        If you stop the crawler now, you can either reinitialize the crawler or resume later at the current state. 
        <asp:Button id="StopButton" OnClick="StopButton_Click" Text="Stop" runat="server" />
    </p>
</asp:Panel>