﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Team_3_Website.CrawlerReference;

namespace Team_3_Website.CrawlerWS
{
    public partial class WS_Control : System.Web.UI.UserControl
    {
        CrawlerServiceClient client;
        private String urlFromConf;

        protected void Page_Load(object sender, EventArgs e)
        {
            client = new CrawlerServiceClient();
            String path = Server.MapPath("~") + @"CrawlerWS\urlConf.xml";
            urlFromConf = XElement.Load(path).Element("url").Value;

            if (!IsPostBack)
            {
                if ((urlFromConf != ""))
                {
                    urlInput.Text = urlFromConf;
                }


                updateLabels();
            }
        }

        protected void InitButton_Click(object sender, EventArgs e)
        {
            bool success = client.InitCrawling(urlInput.Text);
            if (success)
                SuccessLabel.Text = "Crawler was successfully initialized!";
            else
                ErrorLabel.Text = "Initializing the crawler was not successful!";

            updateLabels();
        }

        protected void StartButton_Click(object sender, EventArgs e)
        {
            bool success = client.StartCrawling();
            if (success)
                SuccessLabel.Text = "Crawler was successfully started!";
            else
                ErrorLabel.Text = "Starting the crawler was not successful!";

            updateLabels();
        }

        protected void StopButton_Click(object sender, EventArgs e)
        {
            bool success = client.StopCrawling();
            if (success)
                SuccessLabel.Text = "Crawler was successfully stopped!";
            else
                ErrorLabel.Text = "Stopping the crawler was not successful!";

            updateLabels();
        }

        private void updateLabels()
        {
            if (!client.IsInitialized())
            {
                InitPanel.Visible = true;
                Empty.Visible = true;
                StartPanel.Visible = false;
                StopPanel.Visible = false;
            }
            else if (client.IsCrawling())
            {
                InitPanel.Visible = false;
                Empty.Visible = false;
                StartPanel.Visible = false;
                StopPanel.Visible = true;
            }
            else
            {
                InitPanel.Visible = true;
                Empty.Visible = false;
                StartPanel.Visible = true;
                StopPanel.Visible = false;
            }
        }
    }
}