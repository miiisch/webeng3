﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Team_3_Website.CrawlerWS
{
    public class Link
    {
        private String url;
        private String headline;
        private String content;

        public String Url { get; set; }
        public String Headline { get; set; }
        public String Content { get; set; }

        public Link(String _url, String _headline, String _content)
        {
            this.url = _url;
            this.headline = _headline;
            this.content = _content;
        }

        public void stripContentFromHTML()
        {
            //TODO delete all HTML Tags from Content
        }
    }
}