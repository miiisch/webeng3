﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Team_3_Website
{
    public class CrawlerLogDataSource
    {
        DataClassesDataContext db;

        public class Filter
        {
            public Filter()
            {
                logTypes = new List<CrawlerWebService.Log.LogType>();
                minTimestamp = DateTime.Now.AddHours(-1);
                maxTimestamp = DateTime.Now;
            }

            public Filter(bool init, bool start, bool stop, bool added, bool crawled, DateTime min, DateTime max)
            {
                logTypes = new List<CrawlerWebService.Log.LogType>();
                if (init) logTypes.Add(CrawlerWebService.Log.LogType.Init);
                if (start) logTypes.Add(CrawlerWebService.Log.LogType.Start);
                if (stop) logTypes.Add(CrawlerWebService.Log.LogType.Stop);
                if (added) logTypes.Add(CrawlerWebService.Log.LogType.LinkAdded);
                if (crawled) logTypes.Add(CrawlerWebService.Log.LogType.LinkCrawled);

                minTimestamp = min;
                maxTimestamp = max;
            }

            public List<CrawlerWebService.Log.LogType> logTypes;
            public DateTime minTimestamp;
            public DateTime maxTimestamp;
        }
        public Filter filter { get; set; }
        

        public CrawlerLogDataSource()
        {
            db = new DataClassesDataContext();
            filter = new Filter();
        }

        public DataTable select()
        {
            var result = db.crawler_Logs
                .Where(row => filter.logTypes.Contains(row.logtype) && row.timestamp >= filter.minTimestamp && row.timestamp <= filter.maxTimestamp)
                .OrderByDescending(row => row.id); // ordering by timestamp is not exact enough (more than one entry per second)

            DataTable table = new DataTable();
            table.Columns.Add("Timestamp", typeof(string));
            table.Columns.Add("Action", typeof(string));
            table.Columns.Add("Info", typeof(string));

            foreach (crawler_Log row in result)
            {
                string timestamp = row.timestamp.ToString();
                string action = CrawlerWebService.Log.LogTypeToString(row.logtype);
                String info = String.Empty;

                if (!row.success)
                    info += "[UNSUCCESSFUL] ";

                switch (row.logtype)
                {
                    case CrawlerWebService.Log.LogType.Init:
                        info += "Seed: " + row.url;
                        break;
                    case CrawlerWebService.Log.LogType.Start:
                        break;
                    case CrawlerWebService.Log.LogType.Stop:
                        break;
                    case CrawlerWebService.Log.LogType.LinkAdded:
                        info += row.url;
                        break;
                    case CrawlerWebService.Log.LogType.LinkCrawled:
                        info += row.url + " [" + row.crawlername + "]";
                        break;
                }

                table.Rows.Add(timestamp, action, info);
            }

            return table;
        }
    }
}