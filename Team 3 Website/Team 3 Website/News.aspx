﻿<%@ Page Title="News" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="Team_3_Website.News" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="Content/News.css">
</asp:Content>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    Auf dieser Seite findet man die neusten Neuigkeiten zu unserem Team
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ListView ID="NewsListView" runat="server" DataSourceID="NewsDataSource" DataKeyNames="id">
        
        <LayoutTemplate>
            <div id="itemPlaceholder" runat="server"></div>
            <asp:LoginView ID="InsertButtonLoginView" runat="server">
                <LoggedInTemplate>
                    <asp:PlaceHolder runat="server" ID="InsertButton">
                        <div class="list-item"><div class="list-body-only">
                            <asp:LinkButton ID="LinkButton1" runat="server" Text="New News" OnClick="AddInsert"></asp:LinkButton>
                        </div></div>
                    </asp:PlaceHolder>
                </LoggedInTemplate>
            </asp:LoginView>
        </LayoutTemplate>

        <ItemTemplate>
            <div class="list-item">
                <div class="list-header">
                    <div class="list-header-left news-title"><%# Eval("title")%></div>
                    <div class="list-header-left">by <%# Eval("author")%></div>
                    <div class="list-header-right"><%# Eval("timestamp")%></div>
                    <asp:LoginView ID="LoginView1" runat="server">
                        <LoggedInTemplate>
                            <div class="list-header-right">
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                            </div>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
                <div class="list-body">
                    <%# Eval("newstext")%>
                </div>
            </div>
        </ItemTemplate>
        
        <EmptyDataTemplate>
            There are no news!
        </EmptyDataTemplate>

        <EditItemTemplate>
            <div class="list-item list-edit">
                <div class="list-header">
                    <div class="list-header-left news-title">
                        <asp:TextBox ID="TitleTextbox" runat="server" Text='<%# Bind("title")%>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TitleTextbox"
                            Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </div>
                    <div class="list-header-left">by</div>
                    <div class="list-header-left">
                        <asp:TextBox ID="AuthorTextbox" runat="server" Text='<%# Bind("author")%>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="AuthorTextbox"
                            Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="list-body">
                    <div class="list-content">
                        <asp:TextBox ID="NewstextTextbox" runat="server" Text='<%# Bind("newstext")%>' 
                            TextMode="MultiLine" CssClass="textarea"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="NewstextTextbox"
                            Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </div>
                    <div class="list-buttons">
                        <asp:Button ID="Button1" runat="server" CommandName="Update" Text="OK" ValidationGroup="val"></asp:Button>
                        <asp:Button ID="Button2" runat="server" CommandName="Cancel" Text="Cancel"></asp:Button>
                    </div>
                </div>
            </div>
        </EditItemTemplate>

        <InsertItemTemplate>
            <div class="list-item list-edit">
                <div class="list-header">
                    <div class="list-header-left news-title">
                        <asp:TextBox ID="TitleTextbox" runat="server" Text='<%# Bind("title")%>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TitleTextbox"
                            Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </div>
                    <div class="list-header-left">by</div>
                    <div class="list-header-left">
                        <asp:TextBox ID="AuthorTextbox" runat="server" Text='<%# Bind("author")%>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="AuthorTextbox"
                            Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="list-body">
                    <div class="list-content">
                        <asp:TextBox ID="NewstextTextbox" runat="server" Text='<%# Bind("newstext")%>' 
                            TextMode="MultiLine" CssClass="textarea"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="NewstextTextbox"
                            Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </div>
                    <div class="list-buttons">
                        <asp:Button ID="Button3" runat="server" CommandName="Insert" Text="OK" OnClick="RemoveInsert" ValidationGroup="val"></asp:Button>
                        <asp:Button ID="Button4" runat="server" Text="Cancel" OnClick="RemoveInsert"></asp:Button>
                    </div>
                </div>
            </div>
        </InsertItemTemplate>

    </asp:ListView>



    <asp:SqlDataSource ID="NewsDataSource" runat="server" 
        ConnectionString="<%$ connectionStrings:RemoteSqlConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="SELECT * FROM [news] ORDER BY timestamp DESC"
        UpdateCommand="UPDATE [news] SET title=@title, author=@author, timestamp=GETDATE(), newstext=@newstext WHERE id=@id"
        DeleteCommand="DELETE FROM [news] WHERE id=@id"
        InsertCommand="INSERT INTO [news] (title, author, timestamp, newstext) VALUES (@title, @author, GETDATE(), @newstext)">
    </asp:SqlDataSource>
</asp:Content>