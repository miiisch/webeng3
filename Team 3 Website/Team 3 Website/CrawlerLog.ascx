﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrawlerLog.ascx.cs" Inherits="Team_3_Website.CrawlerLog" %>

<asp:LinqDataSource ID="LogDataSource2" runat="server" ContextTypeName="Team_3_Website.DataClassesDataContext" EntityTypeName="" TableName="crawler_Log"></asp:LinqDataSource>
<asp:ObjectDataSource ID="LogDataSource" runat="server" TypeName="Team_3_Website.CrawlerLogDataSource" OnObjectCreating="LogDataSource_ObjectCreating" SelectMethod="Select"></asp:ObjectDataSource>

<div style="background: #dddddd; border-radius: 15px; padding: 15px; display:block">
    <h2>Filter</h2>

    <h3>Log Types</h3>
    <div><asp:CheckBox ID="InitCheckBox" runat="server" Text="Init" Checked="true" /></div>
    <div><asp:CheckBox ID="StartCheckBox" runat="server" Text="Start" Checked="true" /></div>
    <div><asp:CheckBox ID="StopCheckBox" runat="server" Text="Stop" Checked="true" /></div>
    <div><asp:CheckBox ID="LinkCrawledCheckBox" runat="server" Text="Link Crawled" Checked="true" /></div>
    <div><asp:CheckBox ID="LinkAddedCheckBox" runat="server" Text="Link Added" /></div>

    <h3>Time span</h3>
    <asp:TextBox ID="MinTimestampTextBox" runat="server" Width="155"></asp:TextBox>
    -
    <asp:TextBox ID="MaxTimestampTextBox" runat="server" Width="155"></asp:TextBox>


    <div><asp:Button ID="UpdateFiltersButton" runat="server" Text="Update" OnClick="UpdateFiltersButton_Click" /></div>
</div>

<div><asp:Label ID="CountLabel" runat="server"></asp:Label></div>
<asp:GridView ID="GridView1" runat="server" DataSourceID="LogDataSource" AllowPaging="true" PageSize="200">
</asp:GridView>