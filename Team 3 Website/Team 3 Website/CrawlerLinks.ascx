﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrawlerLinks.ascx.cs" Inherits="Team_3_Website.CrawlerLinks" %>

<asp:LinqDataSource ID="LinksDataSource" runat="server" ContextTypeName="Team_3_Website.DataClassesDataContext" EntityTypeName="" TableName="crawler_Uris"></asp:LinqDataSource>
<div><asp:Label ID="CountLabel" runat="server"></asp:Label></div>
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="uri" DataSourceID="LinksDataSource" AllowPaging="true" PageSize="200">
    <Columns>
        <asp:BoundField DataField="uri" HeaderText="URL" SortExpression="uri" />
        <asp:BoundField DataField="lastCrawled" HeaderText="Last crawled" SortExpression="lastCrawled" />
    </Columns>
</asp:GridView>

