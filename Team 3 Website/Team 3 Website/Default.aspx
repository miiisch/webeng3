﻿<%@ Page Title="Willkommen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Team_3_Website._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    Bei dieser Aufgabe geht es darum eine Teamwebseite zu erstellen, die Seiten zu dem Teammitgliedern, einen News-Bereich
    und eine Link-Unterseite enthält.
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>Die folgenden Dinge wurden im wesentlichen gemacht:</h3>
    <ol class="round">
        <li class="one">
            <h5>Master Page</h5>
            Die Masterpage wurde angepasst (und es wurde ein Logo eingesetzt). 
        </li>
        <li class="two">
            <h5>Menü</h5>
            Es wurde ein eigenes Menü zu Navigation, mit Unterpunkten, erstellt.
        </li>
        <li class="three">
            <h5>News</h5>
            Die Newsseite enthält Einträge, die aus einer Datenbank geladen werden.
        </li>
        <li class="four">
            <h5>Links</h5>
            Die Links werden ebenfalls aus der Datenbank geladen.
        </li>
    </ol>
</asp:Content>
