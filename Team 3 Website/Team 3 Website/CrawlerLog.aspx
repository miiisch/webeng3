﻿<%@ Page Title="Crawler Log" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrawlerLog.aspx.cs" Inherits="Team_3_Website.CrawlerLog1" %>
<%@ Register TagPrefix="My" TagName="CrawlerLog" Src="~/CrawlerLog.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    Auf dieser Seite werden die Logs vom Crawler angezeigt. 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <My:CrawlerLog ID="CrawlerLog2" runat="server"/>
</asp:Content>
