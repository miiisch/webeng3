﻿<%@ Page Title="Crossword" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Team_3_Website.Crossword1" %>
<%@ Register TagPrefix="My" TagName="Crossword" Src="~/Crossword/Crossword.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="Crossword.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="CountLabel" runat="server"></asp:Label>
    <My:Crossword runat="server"/>
</asp:Content>
