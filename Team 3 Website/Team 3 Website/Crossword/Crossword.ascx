﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Crossword.ascx.cs" Inherits="Team_3_Website.Crossword.Crossword" %>
<SCRIPT src="../Scripts/crosswords.js"></SCRIPT>

<asp:Table ID="Table" runat="server" CssClass="crossword"></asp:Table>
<div class="tbs">
    <asp:Button ID="CheckButton" runat="server" Text="Lösung überprüfen" OnClick="CheckButton_Click" />
    <asp:Button ID="SolveButton" runat="server" Text="Lösung zeigen" OnClick="SolveButton_Click" />
</div>