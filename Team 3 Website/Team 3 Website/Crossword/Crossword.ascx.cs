﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Team_3_Website.Crossword
{
    class CrosswordEntry {
        public CrosswordEntry(String Word, String Description)
        {
            this.Word = Word;
            this.Description = Description;
            this.TextBoxes = new List<TextBox>();
        }

        public String Word { get; private set; }
        public String Description { get; private set; }
        public int ShiftWordRight { get; set; }
        public int SolutionIndex { get; private set; }

        public List<TextBox> TextBoxes { get; set; }
        public PlaceHolder ResultLabel { get; set; }

        public CrosswordEntry Clone()
        {
            return new CrosswordEntry(this.Word, this.Description);
        }

        public void SetSolutionIndex(char c, Random rand)
        {
            List<int> possibleSolutions = new List<int>();
            for (int i = 0; i < Word.Length; ++i)
                if (Word[i] == c)
                    possibleSolutions.Add(i);

            int solution = possibleSolutions[rand.Next(possibleSolutions.Count)];

            SolutionIndex = solution;
        }
    }

    public partial class Crossword : System.Web.UI.UserControl
    {
        private DataClassesDataContext db;
        private Random rand;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private String solution;
        private List<CrosswordEntry> words;
        private int width;

        private void buildCrossword()
        {
            // get a solution word
            solution = fetchRandomWord(10).Word;
            words = new List<CrosswordEntry>();

            // get all other words
            foreach (char c in solution)
            {
                CrosswordEntry entry;
                do
                    entry = fetchRandomWord(0, 15);
                while (!entry.Word.Contains(c));

                entry.SetSolutionIndex(c, rand);

                words.Add(entry);
            }

            int maxShift = 0;
            foreach (CrosswordEntry ce in words)
                maxShift = Math.Max(maxShift, ce.SolutionIndex);

            // add maxShiftLeft to each word
            // get the width of the whole puzzle
            width = 0;
            foreach (CrosswordEntry ce in words)
            {
                ce.ShiftWordRight = maxShift - ce.SolutionIndex;
                width = Math.Max(width, ce.ShiftWordRight + ce.Word.Length);
            }


            Session["solution"] = solution;
            Session["words"] = words;
            Session["width"] = width;
        }

        private CrosswordEntry fetchRandomWord(int minLength = 0, int maxLength = 0)
        {
            /*var query = from x in db.crawler_Words select x;
            if (minLength > 0)
                query = query.Where(row => row.word.Length >= minLength);
            if (maxLength > 0)
                query = query.Where(row => row.word.Length <= maxLength);*/

            int count = db.crawler_Words.Count();
            int index = rand.Next(count);
            crawler_Word word = db.crawler_Words.Skip(index).First();

            return new CrosswordEntry(word.word, word.description);
        }

        protected override void CreateChildControls()
        {
            if (!IsPostBack)
            {
                db = new DataClassesDataContext();
                rand = new Random();
                buildCrossword();
            }
            else if (Session["width"] == null)
            {
                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                Label label = new Label();
                label.Text = "Session is empty!";

                cell.Controls.Add(label);
                row.Controls.Add(cell);
                Table.Controls.Add(row);
                return;
            }
            else
            {
                solution = (String)Session["solution"];
                words = (List<CrosswordEntry>)Session["words"];
                width = (int)Session["width"];
            }

            int rowNum = 0;
            foreach(CrosswordEntry entry in words)
            {
                TableRow row = new TableRow();

                TableCell descriptionCell = new TableCell();
                descriptionCell.Text = entry.Description;
                descriptionCell.CssClass = "description";
                row.Controls.Add(descriptionCell);

                int col = 0;

                // add empty cells before the word
                while (col < entry.ShiftWordRight)
                {
                    TableCell cell = new TableCell();
                    row.Controls.Add(cell);

                    ++col;
                }

                // add the word
                int charNum = 0;
                entry.TextBoxes.Clear();
                foreach (char c in entry.Word)
                {
                    TableCell cell = new TableCell();
                    TextBox box = new TextBox();
                    box.ClientIDMode = ClientIDMode.Static;
                    box.ID = "letter" + rowNum + ":" + charNum;
                    if (charNum + 1 < entry.Word.Count())
                        box.Attributes.Add("onKeyUp", "jumpToNextLetter('letter" + rowNum + ":" + (charNum + 1) + "')");
                    box.Attributes.Add("onClick", "this.select()");


                    cell.Controls.Add(box);
                    row.Controls.Add(cell);
                    entry.TextBoxes.Add(box);

                    box.CssClass = "letter";
                    if (col == entry.SolutionIndex + entry.ShiftWordRight)
                        box.CssClass += " solution";
                    box.MaxLength = 1;

                    ++col;
                    ++charNum;
                }                

                // add empty cells after the word
                while (col < width)
                {
                    TableCell cell = new TableCell();
                    row.Controls.Add(cell);

                    ++col;
                }

                PlaceHolder resultLabel = new PlaceHolder();
                entry.ResultLabel = resultLabel;
                TableCell resultCell = new TableCell();
                resultCell.CssClass = "result-check";
                resultCell.Controls.Add(resultLabel);
                row.Controls.Add(resultCell);

                Table.Controls.Add(row);
                ++rowNum;
            }
        }

        protected void CheckButton_Click(object sender, EventArgs e)
        {
            int rowNum = 0;
            foreach(CrosswordEntry entry in words)
            {
                bool correct = true;
                int charNum = 0;
                foreach (char c in entry.Word)
                {
                    TextBox box = entry.TextBoxes[charNum];
                    if (box.Text.ToUpper() != c.ToString().ToUpper())
                        correct = false;

                    ++charNum;
                }

                Image image = new Image();
                image.ImageUrl = correct ? "~/Images/correct.png" : "~/Images/error.png";
                entry.ResultLabel.Controls.Add(image);

                ++rowNum;
            }
        }

        protected void SolveButton_Click(object sender, EventArgs e)
        {
            int rowNum = 0;
            foreach (CrosswordEntry entry in words)
            {
                int charNum = 0;
                foreach (char c in entry.Word)
                {
                    TextBox box = entry.TextBoxes[charNum];
                    box.Text = c.ToString().ToUpper();

                    ++charNum;
                }

                entry.ResultLabel.Controls.Clear();

                ++rowNum;
            }
        }
    }
}