﻿<%@ Page Title="News" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GolemNews.aspx.cs" Inherits="Team_3_Website.GolemNews" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="Content/News.css">
</asp:Content>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    Auf dieser Seite findet man die neusten Neuigkeiten zu unserem Team
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div><asp:Label ID="CountLabel" runat="server"></asp:Label></div>

    <asp:ListView ID="NewsListView" runat="server" DataSourceID="NewsDataSource">
        
        <LayoutTemplate>
            <div id="ItemPlaceholder" runat="server"></div>
            <asp:DataPager ID="DataPager1" runat="server">  
                <Fields>
                    <asp:NumericPagerField /> 
                </Fields>  
            </asp:DataPager> 
        </LayoutTemplate>

        <ItemTemplate>
            <div class="list-item">
                <div class="list-header">
                    <div>
                        <div class="list-header-left"><%# Eval("subheadline")%></div>
                        <div class="list-header-right"><%# Eval("datePublished")%></div>
                    </div>
                    <div class="clear-fix"></div>
                    <div style="margin-top: 10px">
                        <div class="list-header-left news-title"><%# Eval("headline")%></div>
                        <div class="list-header-right">by <%# Eval("author")%></div>
                    </div>
                </div>
                <div class="list-body">
                    <%# Eval("article")%>
                </div>
            </div>
        </ItemTemplate>
        
        <EmptyDataTemplate>
            There are no news!
        </EmptyDataTemplate>

    </asp:ListView>

    <asp:LinqDataSource ID="NewsDataSource" runat="server" ContextTypeName="Team_3_Website.DataClassesDataContext" EntityTypeName="" TableName="crawler_Golem_News"></asp:LinqDataSource>

</asp:Content>