﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParserControl.ascx.cs" Inherits="Team_3_Website._CrawlerTest.ParserControl" %>

<h3>Parser Control:</h3>
<asp:Table ID="Table1" runat="server">
    <asp:TableRow>
        <asp:TableCell><asp:Label ID="lblURL" runat="server" Text="Label">URL: </asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox id="urlInput" Text="" runat="server"/></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell><asp:Label ID="lblCSS" runat="server" Text="Label">Search String: </asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox id="cssTagInput" Text="" runat="server" /></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> </asp:TableCell>
        <asp:TableCell><asp:Button id="parseURL" OnClick="btnGetURLs_Click" Text="get URLs" runat="server" /></asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Team_3_Website._CrawlerTest.ParserControl" SelectMethod="getLinksAsTable" OnObjectCreating="ObjectDataSource1_ObjectCreating"></asp:ObjectDataSource>
<asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1">
</asp:GridView>