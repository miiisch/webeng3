﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using HtmlAgilityPack;

namespace Team_3_Website._CrawlerTest
{
    public class parser
    {
        private HtmlDocument page;

       public parser(HtmlDocument _page)
        {
            this.page = _page;
        }

        public List<String> getLinks()
        {
            List<String> links = new List<string>();
            
            foreach(HtmlNode link in page.DocumentNode.SelectNodes("//a[@href]"))
            {
                links.Add(link.GetAttributeValue("href", "NO_REFERENCE"));
            }

            return links;
        }
    }
}