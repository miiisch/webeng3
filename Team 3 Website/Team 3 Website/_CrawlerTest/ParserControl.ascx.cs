﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using HtmlAgilityPack;
using System.Xml.Linq;
using System.Net;
using System.Data;
using System.ComponentModel;

namespace Team_3_Website._CrawlerTest
{
    [DataObject(true)]
    public partial class ParserControl : System.Web.UI.UserControl
    {
        private String urlFromConf;
        private String urlFromInput;
        private String urlFromInputStripped;
        private List<String> links = new List<String>();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            String path = Server.MapPath("~") + "_CrawlerTest\\urlConf.xml";
            urlFromConf = XElement.Load(path).Element("url").Value;

            if (!IsPostBack)
            {
                if (!(urlFromConf == ""))
                {
                    urlInput.Text = urlFromConf;
                }
                //TODO get SearchString

                //GridView1.Visible = false;
            }
            else
            {
                //GridView1.Visible = true;
            }
        }

        protected void btnGetURLs_Click(object sender, EventArgs e)
        {
            try
            {
                urlFromInput = urlInput.Text; 
            }
            catch (Exception) //TODO Try-Catch, falls falsche URL
            { }
            stripURL(); // for later processing
            HtmlDocument page = fetchPage(urlFromInput);
            parser parser = new parser(page);
            links = parser.getLinks(); //TODO Suchstring übernehmen 
            getCompleteLinks(); //if they are relative

            GridView1.DataBind();
        }

        private HtmlDocument fetchPage(String _url)
        {
            _url = validateURL(_url);
            var uri = new Uri(_url); //TODO can throw an exception if is incorrect URI
            var request = (HttpWebRequest)WebRequest.Create(uri);
            var cookieContainer = new CookieContainer();

            request.CookieContainer = cookieContainer;
            //request.UserAgent = @"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5";
            request.UserAgent = @"Web Engineering - Team 3: Web Crawler";
            request.Method = "GET";
            request.AllowAutoRedirect = true;
            request.Timeout = 15000;

            var response = (HttpWebResponse)request.GetResponse();
            var page = new HtmlDocument();
            page.OptionReadEncoding = false;
            var stream = response.GetResponseStream();
            page.Load(stream); 
            
            return page;
        }

        public List<String> Links
        {
            get { return links; }
            //set { url = value; }
        }

        private String validateURL(String _url)
        {
            if(!_url.StartsWith("http"))
            {
                return "http://" + _url;
            }
            else 
            {
                return _url;
            }
        }

        /**
         * with http in front instead of just /
         * no self-references with #
         * no duplicates
         * no external links
         */
        private void getCompleteLinks()
        {
            List<String> completeLinks = new List<String>();
            String newLink;
            
            foreach(String link in links)
            {
                newLink = "";

                switch (link.ElementAt(0))
                {
                    case '/':
                        newLink = validateURL(urlFromInput + link);
                        break;
                    case '#':
                        // do nothing, self-reference
                        break;
                    default:
                        newLink = link;
                        break;
                }

                if(!completeLinks.Contains(newLink) & !(newLink == "") & !isExternalLink(newLink))
                {
                    completeLinks.Add(newLink);
                }
            }

            links = completeLinks;
        }

        private Boolean isExternalLink(String newLink)
        {
            if (!newLink.Contains(urlFromInputStripped))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void stripURL()
        {
            String strip = "";
            
            if(urlFromInput.StartsWith("https"))
            {
                int startIndex = 8; // after "https://"
                int length = urlFromInput.Length - startIndex-1;
                strip = urlFromInput.Substring(startIndex,length);
            }

            if (urlFromInput.StartsWith("http"))
            {
                int startIndex = 7; // after "http://"
                int length = urlFromInput.Length - startIndex - 1;
                strip = urlFromInput.Substring(startIndex, length);
            }

            if (strip.StartsWith("www."))
            {
                int startIndex = 4; // after "www."
                int length = strip.Length - startIndex - 1;
                strip = strip.Substring(startIndex, length);
            }
            else
            {
                strip = urlFromInput;
            }

            urlFromInputStripped = strip;
        }

        public DataTable getLinksAsTable()
        {
            DataTable results = new DataTable();
            results.Columns.Add("Link");

            foreach (String link in links)
            {
                results.Rows.Add(link);
            }

            return results;
        }

        protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this;
        }
    }
}