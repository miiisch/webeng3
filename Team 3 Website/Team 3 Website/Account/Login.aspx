﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Team_3_Website.Account.Login" %>

<asp:Content runat="server" ContentPlaceHolderID="FeaturedContent">
    Hier kann man sich einloggen
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Login ID="Login1" runat="server" MembershipProvider="SqlMembershipProvider" DestinationPageUrl="~/">
    </asp:Login>
</asp:Content>
