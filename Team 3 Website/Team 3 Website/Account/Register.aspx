﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Team_3_Website.Account.Register" %>

<asp:Content runat="server" ContentPlaceHolderID="FeaturedContent">
    Erstelle hier ein Konto für diese Webseite. 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" MembershipProvider="SqlMembershipProvider"
        CancelDestinationPageUrl="~/" ContinueDestinationPageUrl="~/">
        <WizardSteps>
            <asp:CreateUserWizardStep runat="server" />
            <asp:CompleteWizardStep runat="server" />
        </WizardSteps>
    </asp:CreateUserWizard>
</asp:Content>
