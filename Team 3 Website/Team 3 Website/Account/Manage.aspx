﻿<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="Team_3_Website.Account.Manage" %>

<asp:Content ContentPlaceHolderID="FeaturedContent" runat="server">
    Hier kannst du dein Passwort ändern
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ChangePassword ID="ChangePassword1" runat="server" 
        ContinueDestinationPageUrl="~/" CancelDestinationPageUrl="~/"></asp:ChangePassword>
</asp:Content>
