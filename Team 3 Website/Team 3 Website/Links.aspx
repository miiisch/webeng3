﻿<%@ Page Title="Links" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Links.aspx.cs" Inherits="Team_3_Website.Links" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="Content/Links.css">
</asp:Content>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    Auf dieser Seite findet man Links zu (vielleicht) relevanten Seiten. 
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:ListView ID="NewsListView" runat="server" DataSourceID="LinksDataSource" DataKeyNames="id">
        
        <LayoutTemplate>
            <ul>
                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                <asp:LoginView runat="server">
                    <LoggedInTemplate>
                        <asp:PlaceHolder ID="InsertButton" runat="server">
                            <li>
                                <div class="link-container">
                                    <asp:LinkButton runat="server" Text="New Link" OnClick="AddInsert"></asp:LinkButton>
                                </div>
                            </li>
                        </asp:PlaceHolder>
                    </LoggedInTemplate>
                </asp:LoginView>
            </ul>
        </LayoutTemplate>

        <ItemTemplate>
            <li>
                <div class="link-container">
                    <a href="<%# Eval("link") %>"><%# Eval("title")%></a>
                    <asp:LoginView runat="server">
                        <LoggedInTemplate>
                            (<asp:LinkButton runat="server" CommandName="Edit">Edit</asp:LinkButton>
                            <asp:LinkButton runat="server" CommandName="Delete">Delete</asp:LinkButton>)
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
                <div>
                    <%# Eval("description")%>
                </div>
            </li>

            <!--
            <div class="list-item">
                <div class="list-header">
                    <div class="list-header-left"><a href="<%# Eval("link") %>"><%# Eval("title")%></a></div>
                    <asp:LoginView runat="server">
                        <LoggedInTemplate>
                            <div class="list-header-right">
                                <asp:LinkButton runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="Delete">Delete</asp:LinkButton>
                            </div>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
                <div class="list-body">
                    <%# Eval("description")%>
                </div>
            </div>
            -->
        </ItemTemplate>
        
        <EmptyDataTemplate>
            There are no links!
        </EmptyDataTemplate>

        <EditItemTemplate>
            <li class="undecorated">
                <table>
                    <tr>
                        <td>Title: </td>
                        <td>
                            <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("title")%>'></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="TitleTextBox"
                                Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Link: </td>
                        <td>
                            <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("link")%>'></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="LinkTextBox"
                                Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>
                            <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("description")%>' 
                                TextMode="MultiLine" CssClass="textarea"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="DescriptionTextBox"
                                Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" CommandName="Update" Text="OK" ValidationGroup="val"></asp:Button>
                <asp:Button runat="server" CommandName="Cancel" Text="Cancel"></asp:Button>
            </li>
        </EditItemTemplate>

        <InsertItemTemplate>
            <li class="undecorated">
                <table>
                    <tr>
                        <td>Title: </td>
                        <td>
                            <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("title")%>'></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="TitleTextBox"
                                Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Link: </td>
                        <td>
                            <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("link")%>'></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="LinkTextBox"
                                Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>
                            <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("description")%>' 
                                TextMode="MultiLine" CssClass="textarea"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="DescriptionTextBox"
                                Text="*" CssClass="validationFailed" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" CommandName="Insert" Text="OK" OnClick="RemoveInsert" ValidationGroup="val"></asp:Button>
                <asp:Button runat="server" Text="Cancel" OnClick="RemoveInsert"></asp:Button>
            </li>
        </InsertItemTemplate>

    </asp:ListView>

    <asp:SqlDataSource ID="LinksDataSource" runat="server" 
        ConnectionString="<%$ connectionStrings:RemoteSqlConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="SELECT * FROM [links] ORDER BY id"
        UpdateCommand="UPDATE [links] SET link=@link, title=@title, description=@description WHERE id=@id"
        DeleteCommand="DELETE FROM [links] WHERE id=@id"
        InsertCommand="INSERT INTO [links] (link, title, description) VALUES (@link, @title, @description)">
    </asp:SqlDataSource>
</asp:Content>
